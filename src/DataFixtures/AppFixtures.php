<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Article;
Use App\Entity\Categorie;
Use App\Entity\Commentaire;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $now = date("Y-m-d H:i:s");
        for($i=1 ; $i<=2 ; $i++){
            if($i==1){
                $categorie = new Categorie();
                $categorie->setTitreCategorie('Homme');
                $categorie->setDescription('Categories d\'article pour tou les hommes');
                $manager->persist($categorie);
                //insertion deux aticle pour la categorie homme
                for($j=1 ; $j<=2 ; $j++){
                    if($j == 1){
                        $article = new Article();
                        $article->setTitre('Chaussure');
                        $article->setContenu('Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that could fund your project, and if the buyers liked your flick, they’d pay to Fast-forward and Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that could fund your project, and if the buyers liked your flick, they’d pay to Fast-forward andFilm festivals used to be do-or-die moments for movie makers. They were where you met the producers that could fund your project,Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that could fund your project, and if the buyers liked your flick, they’d pay to Fast-forward and and if the buyers liked your flick, they’d pay to Fast-forward and');
                        $article->setCategorie($categorie);
                        $manager->persist($article);
                        for($k=1 ; $k<=2 ; $k++){
                            if($k==1){
                                $commentaire = new Commentaire();
                                $commentaire->setAuteur('Bernard');
                                $commentaire->setContenuCommentaire('El snort testosterone trophy driving gloves handsome gerry Richardson helvetica tousled street art master testosterone trophy driving gloves handsome gerry Richardson.El snort testosterone trophy driving gloves handsome gerry Richardson helvetica tousled street art master testosterone trophy driving gloves handsome gerry Richardson.El snort testosterone trophy driving gloves handsome gerry Richardson helvetica tousled street art master testosterone trophy driving gloves handsome gerry Richardson');
                                $commentaire->setDateCommentaire(date("m/d/Y h:i:s"));
                                $commentaire->setArticle($article);
                                $manager->persist($commentaire);
                            }else{
                                $commentaire = new Commentaire();
                                $commentaire->setAuteur('Donald Richardson');
                                $commentaire->setContenuCommentaire('Terry Richardson helvetica tousled street art master, El snort testosterone trophy driving gloves handsome letterpress erry Richardson helvetica tousledTerry Richardson helvetica tousled street art master, El snort testosterone trophy driving gloves handsome letterpress erry Richardson helvetica tousled.Terry Richardson helvetica tousled street art master, El snort testosterone trophy driving gloves handsome letterpress erry Richardson helvetica tousled');
                                $commentaire->setDateCommentaire(date("m/d/Y h:i:s"));
                                $commentaire->setArticle($article);
                                $manager->persist($commentaire); 
                            }
                        }
                    }else{
                        $article = new Article();
                        $article->setTitre('Vetement');
                        $article->setContenu('The Buttons extension for DataTables provides a common set of options, API methods and styling to display buttons on a page that will interact with a DataTable. The core library provides the based framework upon which plug-ins can builThe Buttons extension for DataTables provides a common set of options, API methods and styling to display buttons on a page that will interact with a DataTable. The core library provides the based framework upon which plug-ins can builThe Buttons extension for DataTables provides a common set of options, API methods and styling to display buttons on a page that will interact with a DataTable. The core library provides the based framework upon which plug-ins can buil');
                        $article->setCategorie($categorie);
                        $manager->persist($article);
                        for($l=1 ; $l<=2 ; $l++){
                            if($l==1){
                                $commentaire = new Commentaire();
                                $commentaire->setAuteur('Matt');
                                $commentaire->setContenuCommentaire('To all the athaists attacking me right now, I can\'t make you believe in God, you have to have faith. To all the athaists attacking me right now, I can\'t make you believe in God, you have to have faith . To all the athaists attacking me right now, I can\'t make you believe in God, you have to have faith');
                                $commentaire->setDateCommentaire(date("m/d/Y h:i:s"));
                                $commentaire->setArticle($article);
                                $manager->persist($commentaire);
                            }else{
                                $commentaire = new Commentaire();
                                $commentaire->setAuteur('Louis de Gonzag');
                                $commentaire->setContenuCommentaire('Lorem Ipsum is simply dummy text of the printing and Lorem typesetting industry spa.Lorem Ipsum is simply dummy text of the printing and Lorem typesetting industry spa.Lorem Ipsum is simply dummy text of the printing and Lorem typesetting industry spa.Lorem Ipsum is simply dummy text of the printing and Lorem typesetting industry spa');
                                $commentaire->setDateCommentaire(date("m/d/Y h:i:s"));
                                $commentaire->setArticle($article);
                                $manager->persist($commentaire); 
                            }
                        }
                    }
                    
                }
            }else{
                $categorie = new Categorie();
                $categorie->setTitreCategorie('Femme');
                $categorie->setDescription('Categories d\'article pour tou les femmes');
                $manager->persist($categorie);

                //insertion deux aticle pour la categorie homme
                for($k=1 ; $k<=2 ; $k++){
                    if($k == 1){
                        $article = new Article();
                        $article->setTitre('Robe');
                        $article->setContenu('KeyTable provides Excel like cell navigation on any table. Events (focus, blur, action etc) can be assigned to individual cells, columns, rows or all cellsKeyTable provides Excel like cell navigation on any table. Events (focus, blur, action etc) can be assigned to individual cells, columns, rows or all cellsKeyTable provides Excel like cell navigation on any table. Events (focus, blur, action etc) can be assigned to individual cells, columns, rows or all cellsKeyTable provides Excel like cell navigation on any table. Events (focus, blur, action etc) can be assigned to individual cells, columns, rows or all cells');
                        $article->setCategorie($categorie);
                        $manager->persist($article);
                        for($m=1 ; $m<=2 ; $m++){
                            if($m==1){
                                $commentaire = new Commentaire();
                                $commentaire->setAuteur('Lucia Gonzales');
                                $commentaire->setContenuCommentaire(' Ipsum passages and more recently with desktop published software like Aldus PageMaker including versions of Lorem  Ipsum passages and more recently with desktop published software like Aldus PageMaker including versions of Lorem Ipsum passages and more recently with desktop published software like Aldus PageMaker including versions of Lorem');
                                $commentaire->setDateCommentaire(date("m/d/Y h:i:s"));
                                $commentaire->setArticle($article);
                                $manager->persist($commentaire);
                            }else{
                                $commentaire = new Commentaire();
                                $commentaire->setAuteur('Marie');
                                $commentaire->setContenuCommentaire('There are many variations of the passages of Lorem Ipsum onece available, but the majority have suffered alteration in some form, by injected humour There are many variations of the passages of Lorem Ipsum onece available, but the majority have suffered alteration in some form, by injected humour');
                                $commentaire->setDateCommentaire(date("m/d/Y h:i:s"));
                                $commentaire->setArticle($article);
                                $manager->persist($commentaire); 
                            }
                        }
                    }else{
                        $article = new Article();
                        $article->setTitre('Article Beauté');
                        $article->setContenu('Responsive is an extension for DataTables that resolves that problem by optimising the tables layout for different screen sizes through the dynamic insertion and removal of columns from the table.Responsive is an extension for DataTables that resolves that problem by optimising the table layout for different screen sizes through the dynamic insertion and removal of columns from the table.Responsive is an extension for DataTables that resolves that problem by optimising the tables layout for different screen sizes through the dynamic insertion and removal of columns from the table.');
                        $article->setCategorie($categorie);
                        $manager->persist($article);
                        for($m=1 ; $m<=2 ; $m++){
                            if($m==1){
                                $commentaire = new Commentaire();
                                $commentaire->setAuteur('Linda');
                                $commentaire->setContenuCommentaire(' Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatemNemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem');
                                $commentaire->setDateCommentaire(date("m/d/Y h:i:s"));
                                $commentaire->setArticle($article);
                                $manager->persist($commentaire);
                            }else{
                                $commentaire = new Commentaire();
                                $commentaire->setAuteur('Rosie bellamar');
                                $commentaire->setContenuCommentaire('Shabby chic selfies pickled Tumblr letterpress iPhone. Wolf vegan retro selvage literally Wes Anderson ethical four loko. Meggings blog chambray tofu pour-over. Pour-over Tumblr keffiyeh, cornhole whatever cardigan Tonx lomo.Shabby. Shabby chic selfies pickled Tumblr letterpress iPhone. Wolf vegan retro selvage literally Wes Anderson ethical four loko. Meggings blog chambray tofu pour-over. Pour-over Tumblr keffiyeh, cornhole whatever cardigan Tonx lomo.Shabby. Shabby chic selfies pickled Tumblr letterpress iPhone. Wolf vegan retro selvage literally Wes Anderson ethical four loko. Meggings blog chambray tofu pour-over. Pour-over Tumblr keffiyeh, cornhole whatever cardigan Tonx lomo.Shabby');
                                $commentaire->setDateCommentaire(date("m/d/Y h:i:s"));
                                $commentaire->setArticle($article);
                                $manager->persist($commentaire); 
                            }
                        }
                    }
                }
            }
            
        }
        $manager->flush();
    }
}
