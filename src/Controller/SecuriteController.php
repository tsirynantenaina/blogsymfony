<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
Use App\Form\RegistrationType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecuriteController extends AbstractController
{
    #[Route('/registration', name: 'registration')]
    public function registration(Request $request , userPasswordEncoderInterface $encoder){
        $user = new User();

        $form = $this->createForm(RegistrationType::class , $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $password_encoder = $encoder->encodePassword($user , $user->getPassword());
            $user->setPassword($password_encoder);
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('login');
        }
        return $this->render('securite/registration.html.twig', [
            'form'=> $form->createView(),
            'active'=>'registration'
        ]);
    }

    #[Route('/login', name: 'login')]
    public function login(){
        return $this->render('securite/login.html.twig', [
            'active'=>'login'
        ]);
    }

    #[Route('/logout', name: 'logout')]
    public function logout(){
    }
}
