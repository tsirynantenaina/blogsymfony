<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Form\ArticleType;

use App\Entity\Commentaire;
use App\Repository\CommentaireRepository;
use App\Form\CommentaireType;


class ArticleController extends AbstractController
{
    #[Route('/', name: 'article')]
    public function index(ArticleRepository $repo)
    {
        $articles = $repo->findAll();
        return $this->render('article/article.html.twig', [
            'active' => 'article',
            'articles' => $articles,
        ]);
    }

    #[Route('/new', name: 'new')]
    #[Route('/edit/{id}', name: 'edit')]
    public function form(Article $article= null , Request $request )
    {
        if($article == null){
            $article = new Article();
        }  
        $form = $this->createForm(ArticleType::class , $article );
        $form->handleRequest($request); 
        //dump($article);
        if($form->isSubmitTed() && $form->isValid()){
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($article);
            $manager->flush();

            $this->addFlash('noticeArticle', 'Insertion réussie => ID Artice : '.$article->getId().' | Titre article : '.$article->getTitre().' | Contenu : '.$article->getContenu().' ');
            return $this->redirectToRoute('article', [
                
            ]);
        }


        return $this->render('article/form.html.twig', [
            'active' => 'form',
            'formArticle' => $form->createView(),
            'editMode' => $article->getId() != null, 
        ]);
    }

    #[Route('/show/{id}', name: 'show')]
    public function show(Article $article, Request $request){
        $commentaire = new Commentaire();
        $form = $this->createForm(CommentaireType::class, $commentaire);
        
        //on verfifier la requete
        $form->handleRequest($request); 
        if($form->isSubmitTed() && $form->isValid()){
            $manager = $this->getDoctrine()->getManager();
            $commentaire->setDateCommentaire(date("Y-m-d h:i:s"));
            $commentaire->setArticle($article);
            $manager->persist($commentaire);
            $manager->flush();
            return $this->redirectToRoute('show' , [
                'id' => $article->getId(),
            ]); 
            
        }
        return $this->render('article/show.html.twig', [
            'active' => 'article',
            'article' => $article,
            'formCommentaire'=>$form->createView()
        ]);

    }
}
