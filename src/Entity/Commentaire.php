<?php

namespace App\Entity;

use App\Repository\CommentaireRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommentaireRepository::class)
 */
class Commentaire
{
    


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Auteur;

    /**
     * @ORM\Column(type="text")
     */
    private $ContenuCommentaire;

    /**
     * @ORM\Column(type="string" , length=255)
     */
    private $DateCommentaire;

    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="commentaires")
     * @ORM\JoinColumn(nullable=false)
     */
    private $article;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuteur(): ?string
    {
        return $this->Auteur;
    }

    public function setAuteur(string $Auteur): self
    {
        $this->Auteur = $Auteur;

        return $this;
    }

    public function getContenuCommentaire(): ?string
    {
        return $this->ContenuCommentaire;
    }

    public function setContenuCommentaire(string $ContenuCommentaire): self
    {
        $this->ContenuCommentaire = $ContenuCommentaire;

        return $this;
    }

    public function getDateCommentaire(): ?String
    {
        return $this->DateCommentaire;
    }

    public function setDateCommentaire(string $DateCommentaire): self
    {
        $this->DateCommentaire = $DateCommentaire;
        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    
}
